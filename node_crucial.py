"""
Citizens of GridLand are sending emails to each other all the time. They send everything - what they just ate, a funny picture, questions or thoughts that are bothering them right now. All the citizens are happy because they have such a wonderful network that keeps them connected.

The main goal of the Mayor is to control the city's happiness. The city's happiness is a sum of all citizens' happiness. And the happiness of each citizen is equal to the number of citizens (always including oneself) that one can send emails to.

Because the city is growing, the citizens have decided that the Mayor needs an assistant to focus on the node protection.

Your mission is to figure out what will be the first nodes to investigate and protect for the new assistant. Remember, you should choose the most important node in the network. If several nodes have the maximal importance, find all of them
"""
import copy

def calculate_happiness(array, users):
    avg = 0.0
    for x in array:
        if isinstance(x, list):
            temp = 0
            for y in x:
                temp += users[y]
            avg += temp * temp
        else:
            avg += users[x] * users[x]
    return avg


def most_crucial(net, users):
    most_important_node, happiness = '', False
    for x in list(users.keys()):
        damaged_net, queue, temp = [], 1, copy.deepcopy(net)
        while queue:
            y = 0
            while y < len(net):
                if queue == 1:
                    queue = [net[0][0] if net[0][0] != x else net[0][1]]
                    damaged_net.append([net[0][0] if net[0][0] != x else net[0][1]])
                if queue[0] == net[y][0] and net[y][1] != x:
                    queue.append(net[y][1])
                    damaged_net[-1].append(net[y][1])
                    net.pop(y)
                elif queue[0] == net[y][1] and net[y][0] != x:
                    queue.append(net[y][0])
                    damaged_net[-1].append(net[y][0])
                    net.pop(y)
                else:
                    y += 1
            queue.pop(0)
        net = temp
        if not happiness or calculate_happiness(damaged_net, users) < happiness:
            most_important_node, happiness = [x], calculate_happiness(damaged_net, users)
        elif calculate_happiness(damaged_net, users) == happiness:
            most_important_node, happiness = most_important_node + [x], calculate_happiness(damaged_net, users)
    return most_important_node


if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing


    assert most_crucial([
            ['A', 'B']
        ],{
            'A': 20,
            'B': 10
        }) == ['A'], 'Second'

    assert most_crucial([
            ['A', 'B'],
            ['A', 'C'],
            ['A', 'D'],
            ['A', 'E']
        ],{
            'A': 0,
            'B': 10,
            'C': 10,
            'D': 10,
            'E': 10
        }) == ['A'], 'Third'

    assert most_crucial([
            ['A', 'B'],
            ['B', 'C'],
            ['C', 'D']
        ],{
            'A': 10,
            'B': 20,
            'C': 10,
            'D': 20
        }) == ['B'], 'Forth'

    print('Nobody expected that, but you did it! It is time to share it!')