"""
find longest palindrom
"""
def longest_palindromic(a):
    longest = a[0]
    for x in range(len(a)):
        for y in range(len(a) - 1, x, -1):
            if a[x:y+1] == a[y:x:-1] + a[x]:
                if len(a[x:y+1]) > len(longest):
                    longest = a[x:y + 1]
                break
    return longest


if __name__ == '__main__':
    print("Example:")
    print(longest_palindromic('abc'))

    # These "asserts" are used for self-checking and not for an auto-testing
    assert longest_palindromic('abc') == 'a'
    assert longest_palindromic('abacada') == 'aba'
    assert longest_palindromic('artrartrt') == 'rtrartr'
    assert longest_palindromic('aaaaa') == 'aaaaa'
    print("Coding complete? Click 'Check' to earn cool rewards!")
