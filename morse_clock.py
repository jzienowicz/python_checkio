'''
Help Stephen to create a module for converting a normal time string to a morse time string. As you can see in the illustration, a gray circle means on, while a white circle means off. Every digit in the time string contains a different number of slots. The first digit for the hours has a length of 2 while the second digit for the hour has a length of 4. The first digits for the minutes and seconds have a length of 3 while the second digits for the minutes and seconds have a length of 4. Every digit in the time is converted to binary representation. You will convert every on (or 1) signal to dash ("-") and every off (or 0) signal to dot (".").

source: Wikipedia

An time string could be in the follow formats: "hh:mm:ss", "h:m:s" or "hh:m:ss". The "missing" digits are zeroes. For example, "1:2:3" is the same as "01:02:03".

The result will be a morse time string with specific format:
"h h : m m : s s"
where each digits represented as sequence of "." and "-"
'''
def checkio(time_string):
    result = ''
    if ':' in time_string[:2]:
        time_string = '0' + time_string
    for x in range(len(time_string)):
        if time_string[x] is ':':
            result += ': '
            if (x <= 2 and time_string[x + 3] != ':') or x + 2 >= len(time_string):
                result += '... '
        else:
            align = 4 if x == len(time_string) - 1 or time_string[x + 1] == ':' else 3
            if not x:
                align = 2
            result += "{0:0>{align}b} ".format(ord(time_string[x]) - ord('0'), align=align)
    result = result.replace('0', '.').replace('1', '-')[:-1]
    return result


if __name__ == '__main__':
    print("Example:")
    print(checkio("0:37:49"))

    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert checkio("10:37:49") == ".- .... : .-- .--- : -.. -..-", "First Test"
    assert checkio("21:34:56") == "-. ...- : .-- .-.. : -.- .--.", "Second Test"
    assert checkio("00:1:02") == ".. .... : ... ...- : ... ..-.", "Third Test"
    assert checkio("23:59:59") == "-. ..-- : -.- -..- : -.- -..-", "Fourth Test"
    print("Coding complete? Click 'Check' to earn cool rewards!")