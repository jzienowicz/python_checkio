"""
Sometimes damaged nodes are unrecoverable. In that case, people that were connected to the crushed node must migrate to another district while administration attempts to fix the node.

But if a crushed node disconnects multiple districts from one another, then the network splits into two sub-networks and every sub-network should have their own Mayor. And Mayors must use pigeons for mailing between each other. In that case, when the network is split you don’t need hundreds of pigeons.

Your mission is to figure out how many Mayors you need to control the entire city when some nodes are crushed. In other words, you need to figure out how many sub-networks will be formed after some nodes are crush and not recovered.
"""

def subnetworks(net, crushes):
    mayors = 0
    while net:
        queue = [x[0] for x in net if x[0] not in crushes] + [x[1] for x in net if x[1] not in crushes]
        try:
            queue = [queue[0]]
        except:
            return mayors
        mayors += 1
        while queue:
            x = 0
            while x < len(net):
                if queue[0] == net[x][0]:
                    if net[x][1] not in crushes:
                        queue.append(net[x][1])
                    net.pop(x)
                elif queue[0] == net[x][1]:
                    if net[x][0] not in crushes:
                        queue.append(net[x][0])
                    net.pop(x)
                else:
                    x += 1
            queue.pop(0)
    return mayors

if __name__ == '__main__':
    assert subnetworks([
            ['A', 'B'],
            ['B', 'C'],
            ['C', 'D']
        ], ['B']) == 2, "First"
    assert subnetworks([
            ['A', 'B'],
            ['A', 'C'],
            ['A', 'D'],
            ['D', 'F']
        ], ['A']) == 3, "Second"
    assert subnetworks([
            ['A', 'B'],
            ['B', 'C'],
            ['C', 'D']
        ], ['C', 'D']) == 1, "Third"
