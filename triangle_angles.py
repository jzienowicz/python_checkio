"""
You are given the lengths for each side on a triangle. You need to find all three angles for this triangle. If the given side lengths cannot form a triangle (or form a degenerated triangle), then you must return all angles as 0 (zero). The angles should be represented as a list of integers in ascending order. Each angle is measured in degrees and rounded to the nearest integer number (Standard mathematical rounding).
"""
from math import *


def checkio(a, b, c):
    if a + b <= c or a + c <= b or b + c <= a:
        return [0, 0, 0]
    a, b, c = float(a), float(b), float(c)
    cosc = acos((c ** 2 - (a ** 2 + b ** 2)) / (a * b * 2))
    cosb = acos((b ** 2 - (a ** 2 + c ** 2)) / (a * c * 2))
    cosa = acos((a ** 2 - (c ** 2 + b ** 2)) / (c * b * 2))
    return sorted([round(180 - degrees(cosc)), round(180 - degrees(cosb)), round(180 - degrees(cosa))])


# These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    print("Example:")
    print(checkio(4, 4, 4))

    print(checkio(4, 4, 4))
    print(checkio(3, 4, 5))
    print(checkio(2, 2, 5))
    print("Coding complete? Click 'Check' to earn cool rewards!")