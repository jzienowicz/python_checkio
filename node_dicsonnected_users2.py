"""
Welcome to the GridLand. All the citizens here are connected through the global internal network because the main way for communication here is via email. Every new district of the city starts with building a node – center of the district. All citizens are connected to this node in order to send and receive emails. All nodes of GridLand are connected so one node can send emails between the connected nodes. In such a way, no matter how big the city is all users can send messages to each other as long as all of the nodes are connected.

The Mayor of GridLand is using this network to quickly send emergency emails to all citizens when necessary. But the system is not perfect. When one of city nodes gets crushed it may leave the citizens of this node district disconnected from these emergency emails. It may also leave districts around the crushed node disconnected if their nodes do not have other ways to connect. To resolve this occurrence, the Mayor uses mail-pigeons – an old method of sending mail that was invented before the global internal network. All of the citizens still connected to the network receive the emergency emails, but the disconnected citizens receive their messages from these pigeons.

Your mission is to figure out how many pigeons you need when some of the nodes are crushed.
"""
def disconnected_users(net, users, source, crushes):
    net = [x for x in net if x[0] not in crushes and x[1] not in crushes]
    queue = [source] if source not in crushes else []
    not_connected = [x for x in list(users.keys()) if x != source or not queue]
    while queue:
        x = 0
        while x < len(net):
            if queue[0] == net[x][0]:
                if net[x][1] in not_connected:
                    not_connected.remove(net[x][1])
                queue.append(net[x][1])
                net.pop(x)
            elif queue[0] == net[x][1]:
                if net[x][0] in not_connected:
                    not_connected.remove(net[x][0])
                queue.append(net[x][0])
                net.pop(x)
            else:
                x += 1
        queue.pop(0)
    sum = 0
    for x in not_connected:
        sum += users[x]
    return sum

if __name__ == '__main__':
    print(disconnected_users([["A","B"],["A","C"],["A","D"],["A","E"],["A","F"]],{"A":10,"B":10,"C":10,"D":10,"E":10,"F":10},"A",["B","C"]))
    assert disconnected_users([
        ['A', 'B'],
        ['B', 'C'],
        ['C', 'D']
    ], {
        'A': 10,
        'B': 20,
        'C': 30,
        'D': 40
    },
        'A', ['C']) == 70, "First"

    assert disconnected_users([
        ['A', 'B'],
        ['B', 'D'],
        ['A', 'C'],
        ['C', 'D']
    ], {
        'A': 10,
        'B': 0,
        'C': 0,
        'D': 40
    },
        'A', ['B']) == 0, "Second"

    assert disconnected_users([
        ['A', 'B'],
        ['A', 'C'],
        ['A', 'D'],
        ['A', 'E'],
        ['A', 'F']
    ], {
        'A': 10,
        'B': 10,
        'C': 10,
        'D': 10,
        'E': 10,
        'F': 10
    },
        'C', ['A']) == 50, "Third"

    print('Done. Try to check now. There are a lot of other tests')