def power_supply(network, power_plants):
    cities = [y for x in network for y in x]
    cities = list(set(cities))
    while len(power_plants):
        power_plant_name = list(power_plants.keys())[0]
        if power_plant_name in cities:
            cities.remove(power_plant_name)
        if power_plants[power_plant_name] >= 1:
            for x in network:
                if power_plant_name == x[0]:
                    if x[1] in cities:
                        cities.remove(x[1])
                    if power_plants[power_plant_name] != 1:
                        power_plants.update({x[1]: power_plants[power_plant_name] - 1})
                elif power_plant_name == x[1]:
                    if x[0] in cities:
                        cities.remove(x[0])
                    if power_plants[power_plant_name] != 1:
                        power_plants.update({x[0]: power_plants[power_plant_name] - 1})
        power_plants.pop(power_plant_name)
    return set(cities)


if __name__ == '__main__':
    assert power_supply([['p1', 'c1'], ['c1', 'c2']], {'p1': 1}) == set(['c2']), 'one blackout'
    assert power_supply([['c0', 'c1'], ['c1', 'p1'], ['c1', 'c3'], ['p1', 'c4']], {'p1': 1}) == set(
        ['c0', 'c3']), 'two blackout'
    assert power_supply([['p1', 'c1'], ['c1', 'c2'], ['c2', 'c3']], {'p1': 3}) == set([]), 'no blackout'
    assert power_supply([['c0', 'p1'], ['p1', 'c2']], {'p1': 0}) == set(['c0', 'c2']), 'weak power-plant'
    assert power_supply([['p0', 'c1'], ['p0', 'c2'], ['c2', 'c3'], ['c3', 'p4'], ['p4', 'c5']],
                        {'p0': 1, 'p4': 1}) == set([]), 'cooperation'
    assert power_supply([['c0', 'p1'], ['p1', 'c2'], ['c2', 'c3'], ['c2', 'c4'], ['c4', 'c5'],
                         ['c5', 'c6'], ['c5', 'p7']],
                        {'p1': 1, 'p7': 1}) == set(['c3', 'c4', 'c6']), 'complex cities 1'
    assert power_supply([['p0', 'c1'], ['p0', 'c2'], ['p0', 'c3'],
                         ['p0', 'c4'], ['c4', 'c9'], ['c4', 'c10'],
                         ['c10', 'c11'], ['c11', 'p12'], ['c2', 'c5'],
                         ['c2', 'c6'], ['c5', 'c7'], ['c5', 'p8']],
                        {'p0': 1, 'p12': 4, 'p8': 1}) == set(['c6', 'c7']), 'complex cities 2'
    assert power_supply([['c1', 'c2'], ['c2', 'c3']], {}) == set(['c1', 'c2', 'c3']), 'no power plants'
    assert power_supply([['p1', 'c2'], ['p1', 'c4'], ['c4', 'c3'], ['c2', 'c3']], {'p1': 1}) == set(['c3']), 'circle'
    assert power_supply([['p1', 'c2'], ['p1', 'c4'], ['c2', 'c3']], {'p1': 4}) == set([]), 'more than enough'
