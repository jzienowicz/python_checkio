"""
There is a lunch place at your work with the 3 microwave ovens (Мicrowave1, Мicrowave2, Мicrowave3), which are the subclasses of the MicrowaveBase class. Every microwave can be controlled by a RemoteControl. The RemoteControl uses the next commands:

set_time("xx:xx"), where "xx:xx" - time in minutes and seconds, which shows how long the food will be warming up. For example: set_time("05:30");
add_time("Ns"), add_time("Nm"), where N - the number of seconds("s") or minutes("m"), which should be added to the current time;
del_time("Ns"), del_time("Nm"), where N - the amount of the seconds("s") or minutes("m"), which should be subtracted from the current time;
show_time() - shows the current time for the microwave.

The default time is 00:00. The time can't be less than 00:00 or greater than 90:00, even if add_time or del_time causes it.
"""

class MicrowaveBase:
    def __init__(self):
        self.time = [0, 0]

    def show_time(self):
        return '{:02}:{:02}'.format(self.time[0], self.time[1])


class Microwave1(MicrowaveBase):
    def show_time(self):
        return '_{:1}:{:02}'.format(self.time[0] % 10, self.time[1])


class Microwave2(MicrowaveBase):
    def show_time(self):
        return '{:02}:{:1}_'.format(self.time[0], int(self.time[1] / 10))


class Microwave3(MicrowaveBase):
    pass


class RemoteControl:
    def __init__(self, microwave):
        self.microwave = microwave

    def set_time(self, time):
        self.microwave.time = [int(time[:2]), int(time[3:])]

    def add_time(self, time):
        if time[-1] == 's':
            time = int(time[:-1]) + self.microwave.time[1]
            minutes = int(time / 60)
            self.microwave.time = [self.microwave.time[0] + minutes, (time % 60)]
        else:
            self.microwave.time = [self.microwave.time[0] + int(time[:-1]), self.microwave.time[1]]
        if self.microwave.time[0] >= 90:
            self.microwave.time = [90, 0]

    def del_time(self, time):
        if time[-1] == 's':
            time = self.microwave.time[1] - int(time[:-1])
            minutes = int(time / 60)
            self.microwave.time = [self.microwave.time[0] + minutes, abs(time) % 60]
        else:
            self.microwave.time = [self.microwave.time[0] - int(time[:-1]), self.microwave.time[1]]
        if self.microwave.time[0] < 0:
            self.microwave.time = [0, 0]

    def show_time(self):
        return self.microwave.show_time()


if __name__ == '__main__':
    # These "asserts" using only for self-checking and not necessary for auto-testing
    microwave_2 = Microwave2()
    rc_2 = RemoteControl(microwave_2)
    rc_2.set_time("89:00")
    rc_2.add_time("90s")
    rc_2.add_time("20m")
    rc_2.show_time()