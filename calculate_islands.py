"""
The Robots have found a chain of islands in the middle of the Ocean. They would like to explore these islands and have asked for your help calculating the areas of each island. They have given you a map of the territory. The map is a 2D array, where 0 is water, 1 is land. An island is a group of land cells surround by water. Cells are connected by their edges and corners. You should calculate the areas for each of the islands and return a list of sizes (quantity of cells) in ascending order. All of the cells outside the map are considered to be water.
"""

def delete_island(map_list, x, y):
    size = 1
    remember = []
    while True:
        if x < len(map_list) - 1 and map_list[x + 1][y] == 1:
            map_list[x + 1][y] = 0
            size += 1
            remember.append([x, y])
            x += 1
        elif x > 0 and y > 0 and map_list[x - 1][y - 1] == 1:
            map_list[x - 1][y - 1] = 0
            size += 1
            remember.append([x, y])
            x, y = x - 1, y - 1
        elif x > 0 and y < len(map_list[0]) - 1 and map_list[x - 1][y + 1] == 1:
            map_list[x - 1][y + 1] = 0
            size += 1
            remember.append([x, y])
            x, y = x - 1, y + 1
        elif x < len(map_list) - 1 and y > 0 and map_list[x + 1][y - 1] == 1:
            map_list[x + 1][y - 1] = 0
            size += 1
            remember.append([x, y])
            x, y = x + 1, y - 1
        elif x < len(map_list) - 1 and y < len(map_list[0]) - 1 and map_list[x + 1][y + 1] == 1:
            map_list[x + 1][y + 1] = 0
            size += 1
            remember.append([x, y])
            x, y = x + 1, y + 1
        elif x > 0 and map_list[x - 1][y] == 1:
            map_list[x - 1][y] = 0
            size += 1
            remember.append([x, y])
            x -= 1
        elif y < len(map_list[0]) - 1 and map_list[x][y + 1] == 1:
            map_list[x][y + 1] = 0
            size += 1
            remember.append([x, y])
            y += 1
        elif y > 0 and map_list[x][y - 1] == 1:
            map_list[x][y - 1] = 0
            size += 1
            y -= 1
        elif remember:
            x, y = remember[0][0], remember[0][1]
            remember.pop(0)
        else:
            return map_list, size



def checkio(map_list):
    result = []
    for x in range(len(map_list)):
        for y in range(len(map_list[0])):
            if map_list[x][y] == 1:
                map_list[x][y] = 0
                map_list, size = delete_island(map_list, x, y)
                result.append(size)
    return sorted(result)



if __name__ == "__main__":
    print(checkio([[0, 0, 0, 0, 0],
                    [0, 0, 1, 1, 0],
                    [0, 0, 0, 1, 0],
                    [0, 1, 1, 0, 0]]))