

def checkio(first, second):
    first = "{0:b}".format(first)
    second = "{0:b}".format(second)
    s = 0
    a, o, x = [], [], []
    for i in range(len(first)):
        a.append([]), o.append([]), x.append([])
        for j in second:
            a[i].append((ord(j) - ord('0')) and (ord(first[i]) - ord('0')))
            o[i].append((ord(j) - ord('0')) or (ord(first[i]) - ord('0')))
            x[i].append((ord(j) - ord('0')) ^ (ord(first[i]) - ord('0')))
        s += sum([(2 ** (len(a[i]) - z - 1)) for z in range(len(a[i])) if a[i][z]]) + sum([(2 ** (len(o[i]) - z - 1)) for z in range(len(o[i])) if o[i][z]]) + sum([(2 ** (len(x[i]) - z - 1)) for z in range(len(x[i])) if x[i][z]])
    return s

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio(4, 6) == 38
    assert checkio(2, 7) == 28
    assert checkio(7, 2) == 18
