"""
Nicola regularly inspects the local networks for security issues. He uses a smart and aggressive program which takes control of computers on the network. This program attacks all connected computers simultaneously, then uses the captured computers for further attacks. Nicola started the virus program in the first computer and took note of the time it took to completely capture the network. We can help him improve his process by modeling and improving his inspections.

We are given information about the connections in the network and the security level for each computer. Security level is the time (in minutes) that is required for the virus to capture a machine. Capture time is not related to the number of infected computers attacking the machine. Infection start from the 0th computer (which is already infected). Connections in the network are undirected. Security levels are not equal to zero (except infected).

Information about a network is represented as a matrix NxN size, where N is a number of computers. If ith computer connected with jth computer, then matrix[i][j] == matrix[j][i] == 1, else 0. Security levels are placed in the main matrix diagonal, so matrix[i][i] is the security level for the ith computer.
"""
def capture(matrix):
    not_infected = range(1, len(matrix))
    distance = [(x, matrix[x][x]) for x in range(1, len(matrix)) if matrix[0][x]]
    while True:
        distance = sorted(distance, key=lambda kv: kv[1])
        not_infected.remove(distance[0][0])
        if not not_infected:
            return distance[0][1]
        for x in range(len(matrix)):
            if x != distance[0][0] and x in not_infected and matrix[distance[0][0]][x]:
                if x not in [y[0] for y in distance]:
                    distance.append((x, matrix[x][x] + distance[0][1]))
                else:
                    time = [q for q in range(len(distance)) if distance[q][0] == x]
                    if distance[time[0]][1] > matrix[x][x] + distance[0][1]:
                        distance[time[0]][1] = matrix[x][x] + distance[0][1]
        distance.pop(0)


if __name__ == '__main__':
    assert capture([[0, 1, 0, 1, 0, 1],
                    [1, 8, 1, 0, 0, 0],
                    [0, 1, 2, 0, 0, 1],
                    [1, 0, 0, 1, 1, 0],
                    [0, 0, 0, 1, 3, 1],
                    [1, 0, 1, 0, 1, 2]]) == 8, "Base example"
    assert capture([[0, 1, 0, 1, 0, 1],
                    [1, 1, 1, 0, 0, 0],
                    [0, 1, 2, 0, 0, 1],
                    [1, 0, 0, 1, 1, 0],
                    [0, 0, 0, 1, 3, 1],
                    [1, 0, 1, 0, 1, 2]]) == 4, "Low security"
    assert capture([[0, 1, 1],
                    [1, 9, 1],
                    [1, 1, 9]]) == 9, "Small"