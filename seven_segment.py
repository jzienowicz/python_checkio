'''
JA English
You have a device that uses a Seven-segment display to display 2 digit numbers. However, some of the segments aren't working and can't be displayed.

You will be given information on the lit and broken segments. You won't know whether the broken segment is lit or not. You have to count and return the total number that the device may be displaying.

The input is a set of lit segments (the first argument) and broken segments (the second argument).

Uppercase letters represent the segments of the first out two digit number.
Lowercase letters represent the segments of the second out two digit number.
topmost: 'A(a)', top right: 'B(b)', bottom right: 'C(c)', bottommost: 'D(d)', bottom left: 'E(e)', top left: 'F(f)', middle: 'G(g)'
'''
NUMBERS = [['B', 'C'], #1
           ['A', 'B', 'D', 'E', 'G'],#2
           ['A', 'B', 'C', 'D', 'G'],#3
           ['B', 'C', 'F', 'G'],#4
           ['A', 'C', 'D', 'F', 'G'],#5
           ['A', 'C', 'D', 'E', 'F', 'G'],#6
           ['A', 'B', 'C'],#7
           ['A', 'B', 'C', 'D', 'E', 'F', 'G'],#8
           ['A', 'B', 'C', 'D', 'F', 'G'],#9
           ['A', 'B', 'C', 'D', 'E', 'F']] #0


def generate_permutations(array):
    result = []
    for x in range(1, 2 ** len(array)):
        result.append([array[y] for y in range(len(array)) if '{0:0>{align}b}'.format(x, align=len(array))[y] == '1'])
    return result


def seven_segment(lit_seg, broken_seg):
    lit_seg_upper = sorted([x for x in lit_seg if ord(x) < ord('a')])
    lit_seg_lower = sorted([x.upper() for x in lit_seg if ord(x) >= ord('a')])
    broken_seg_upper = sorted([x for x in broken_seg if ord(x) < ord('a')])
    broken_seg_lower = sorted([x.upper() for x in broken_seg if ord(x) >= ord('a')])
    sum_upper, sum_lower = 1 if lit_seg_upper in NUMBERS else 0, 1 if lit_seg_lower in NUMBERS else 0
    for x in generate_permutations(broken_seg_lower):
        if sorted(lit_seg_lower + x) in NUMBERS:
            sum_lower += 1
    for x in generate_permutations(broken_seg_upper):
        if sorted(lit_seg_upper + x) in NUMBERS:
            sum_upper += 1
    return sum_lower * sum_upper


if __name__ == '__main__':
    print(seven_segment({'B', 'C', 'b', 'c'}, {'A'}))
    assert seven_segment({'B', 'C', 'b', 'c'}, {'A'}) == 2, '11, 71'
    assert seven_segment({'B', 'C', 'a', 'f', 'g', 'c', 'd'}, {'A', 'G', 'D', 'e'}) == 6, '15, 16, 35, 36, 75, 76'
    assert seven_segment({'B', 'C', 'a', 'f', 'g', 'c', 'd'}, {'A', 'G', 'D', 'F', 'b', 'e'}) == 20, '15...98'
    print('"Run" is good. How is "Check"?')