"""
You are given an expression with numbers, brackets and operators. For this task only the brackets matter. Brackets come in three flavors: "{}" "()" or "[]". Brackets are used to determine scope or to restrict some expression. If a bracket is open, then it must be closed with a closing bracket of the same type. The scope of a bracket must not intersected by another bracket. In this task you should make a decision, whether to correct an expression or not based on the brackets. Do not worry about operators and operands.
"""
OPEN_BRACKETS = ["{", "(", "["]
CLOSED_BRACKETS = ["}", ")", "]"]


def checkio(expression):
    stack = []
    for x in expression:
        if x in OPEN_BRACKETS:
            stack.append(x)
        elif x in CLOSED_BRACKETS:
            if stack and OPEN_BRACKETS.index(stack[-1]) == CLOSED_BRACKETS.index(x):
                stack.pop(-1)
            elif OPEN_BRACKETS[CLOSED_BRACKETS.index(x)] not in stack:
                return False
    return False if stack else True

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    print(checkio("(((([[[{{{3}}}]]]]))))"))
    assert checkio("((5+3)*2+1)") == True, "Simple"
    assert checkio("{[(3+1)+2]+}") == True, "Different types"
    assert checkio("(3+{1-1)}") == False, ") is alone inside {}"
    assert checkio("[1+1]+(2*2)-{3/3}") == True, "Different operators"
    assert checkio("(({[(((1)-2)+3)-3]/3}-3)") == False, "One is redundant"
    assert checkio("2+3") == True, "No brackets, no problem"